import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeFilmComponent } from './liste-film/liste-film.component';
import { ListePersonnelComponent } from './liste-personnel/liste-personnel.component';
import { HeaderComponent } from './header/header.component';
import { FilmdetailsComponent } from './filmdetails/filmdetails.component';
import { FilmService } from './film.service';
import { FavorisService } from './favoris.service';

@NgModule({
  declarations: [
    AppComponent,
    ListeFilmComponent,
    ListePersonnelComponent,
    HeaderComponent,
    FilmdetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    FilmService,
    FavorisService
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }


