import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) { }

  getFilm(){
    return this.http.get('https://api.themoviedb.org/3/discover/movie?api_key=8c41b7fe30078f4211374d86ec032427&language=fr');
  }

  getRechercheFilm(refilm: string){
    return this.http.get('https://api.themoviedb.org/3/search/movie?api_key=8c41b7fe30078f4211374d86ec032427&language=fr&query=' + refilm);
  }

  getDetailFilm(id: number){
    return this.http.get('https://api.themoviedb.org/3/movie/' + id + '?api_key=8c41b7fe30078f4211374d86ec032427&language=fr');
  }
}
