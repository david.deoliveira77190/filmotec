import { Injectable } from '@angular/core';
import { Favoris } from './favoris';

@Injectable({
  providedIn: 'root'
})
export class FavorisService {

  private favoris: Favoris[];
  tabFavori!: string;

  constructor() {
    this.favoris = [];
  }

  getAllFavoris() {
    this.favoris = JSON.parse(localStorage.getItem('Favoris') || '[]');
    return this.favoris;
  }

  addFavoris(favori: Favoris) {
    this.favoris.push(favori);

    this.tabFavori = JSON.stringify(this.favoris);
    localStorage.setItem('Favoris', this.tabFavori);

  }

  deleteFavoris(id: number) {
    if (id == 1) {
      this.favoris.splice(0, id);
      this.tabFavori = JSON.stringify(this.favoris);
      localStorage.setItem('Favoris', this.tabFavori);
    } else {
      this.favoris.splice(id - 1, id - 1);
      this.tabFavori = JSON.stringify(this.favoris);
      localStorage.setItem('Favoris', this.tabFavori);
    }
  }

}
