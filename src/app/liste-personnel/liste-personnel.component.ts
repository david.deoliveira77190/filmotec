import { Component, OnInit } from '@angular/core';
import { Favoris } from '../favoris';
import { FavorisService } from '../favoris.service';

@Component({
  selector: 'app-liste-personnel',
  templateUrl: './liste-personnel.component.html',
  styleUrls: ['./liste-personnel.component.scss']
})
export class ListePersonnelComponent implements OnInit {

  favoris: Favoris[];

  constructor(private favori: FavorisService) {
    this.favoris = this.favori.getAllFavoris();
  }

  ngOnInit(): void {
  }

  supprimeFavori(id :number){
    this.favori.deleteFavoris(id);
  }

}
